<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ListController;
use App\Http\Controllers\Auth\loginController;
use App\Http\Controllers\Auth\UserController;
use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\layoutController;
use App\Http\Controllers\nameofController;
use App\Http\Middleware\CheckLogin;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/testuser',[loginController::Class,'testusers']);
Route::post('/testlogin',[loginController::Class,'testlogin'])->name('testlogin');
Route::post('/testauth',[loginController::Class,'testauthUser']);
Route::get('/logout',[loginController::Class,'testuserLogout']);
Route::get('/home',[loginController::Class,'home']);



Route::get('/login',[loginController::Class,'login'])->name('login');
Route::post('/auth',[loginController::Class,'authUser']);
Route::post('/signup',[loginController::Class,'userSignup']);
Route::get('/signupview',[loginController::Class,'signup']);



   Route::resource('/list',ListController::Class)->middleware('auth');
   Route::any('/deleteuser/{id}',[ListController::Class,'deleteUser'])->middleware('auth');
   Route::get('/logout',[loginController::Class,'userLogout'])->middleware('auth');


  // Route::get('/testdashboad',[loginController::Class,'authenticate']);
  
Route::get('/website',[loginController::Class,'website']);
Route::get('/website/Contect',[loginController::Class,'websiteContect']);
Route::get('/website/About',[loginController::Class,'websiteAbout']);
Route::get('/website/Portfolio',[loginController::Class,'websitePortfolio']);


Route::get('/layout',[layoutController::Class,'layout']);
Route::get('/layout/Feedback',[layoutController::Class,'layoutFeedback']);
Route::get('/layout/About',[layoutController::Class,'layoutAbout']);
// Route::get('/layout/Project',[layoutController::Class,'layoutProject']);
// Route::get('/layout/Contact',[layoutController::Class,'layoutContact']);
Route::get('/layout/AddFeedback',[layoutController::Class,'layoutAddFeedback'])->middleware('auth','CheckLogin');
Route::get('/layout/accountpage',[layoutController::Class,'layoutAccountpage']);

Route::post('/layout/Signup',[layoutController::Class,'layoutSignup']);
Route::post('/layout/store',[layoutController::Class,'storedata']);
Route::post('/layout/Login',[layoutController::Class,'layoutLogin']);
Route::get('/layout/Logout',[layoutController::Class,'layoutLogout'])->middleware('auth');

Route::resource('/array',nameofController::Class);
Route::get('/layout/Project',[layoutController::Class,'layoutProject']);
Route::get('/layout/Contact',[layoutController::Class,'layoutContact']);

// Route::group(["middleware" => ["apprestrict"]], function(){
//   Route::get('/layout/Project',[layoutController::Class,'layoutProject']);
//   Route::get('/layout/Contact',[layoutController::Class,'layoutContact']);
  
// });

Route::resource('/employee',EmployeeController::Class);

Route::any('/deleteEmployee/{id}',[CuserContraller::Class,'deleteEmployee']);


