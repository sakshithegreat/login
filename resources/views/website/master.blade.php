<!doctype html>

<html>

<head>

   @include('layout.head')

</head>

<body id="page-top">

<div class="container">

   <header class="row">
       @include('website.navbar')
       @include('website.header')

   </header>

   <div id="main" class="row">
        
           @yield('content')
          

   </div>

   <footer class="row">

       @include('website.footer')
     

   </footer>

</div>

    </body>

</html>