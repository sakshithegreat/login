<!DOCTYPE html>
<html>

<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<link href='https://fonts.googleapis.com/css?family=Sofia' rel='stylesheet'>
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha256-aAr2Zpq8MZ+YA/D6JtRD3xtrwpEz2IqOS+pWD/7XKIw=" crossorigin="anonymous" />

    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha256-OFRAJNoaD8L3Br5lglV7VyLRf0itmoBzWUoM+Sji4/8=" crossorigin="anonymous"></script>

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>

    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />

    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>


<style>


body {
  margin: 0;
  min-width: 250px;
  font-family: 'Sofia';font-size: 22px;
}

/* Include the padding and border in an element's total width and height */
* {
  box-sizing: border-box;
}

/* Style the close button */
.close {
  position: absolute;
  right: 0;
  top: 0;
  padding: 12px 16px 12px 16px;
}

.close:hover {
  background-color: #f44336;
  color: white;
}

/* Style the header */
.header {
  background-color: #f44336;
  padding: 30px 40px;
  color: white;
  text-align: center;
}

/* Clear floats after the header */
.header:after {
  content: "";
  display: table;
  clear: both;
}

.backBtn{
  padding: 10px;
  width: auto%;
  background: #d9d9d9;
  color: #555;
  float: left;
  text-align: center;
  font-size: 16px;
  cursor: pointer;
  transition: 0.3s;
  border-radius: 0;
}



/* Style the input */
input ,select {
  margin: 5px;
  color:black;
  border: none;
  border-radius: 0;
  width: 30%;
  padding: 10px;
  float: left;
  font-family: 'Sofia';font-size: 22px;
  font-size: 18px;
}

/* Style the "Add" button */
.addBtn {
  padding: 10px;
  width: auto%;
  background: #d9d9d9;
  color: #555;
  float: right;
  text-align: center;
  font-size: 16px;
  cursor: pointer;
  transition: 0.3s;
  border-radius: 0;
}

.addBtn:hover {
  background-color: #bbb;
}
.addSkills input{
  margin: 5px;
  color:black;
  border: none;
  border-radius: 0;
  width: auto;
  padding: 10px;
  float: left;
  font-family: 'Sofia';font-size: 22px;
  font-size: 16px;
}
.shosha{
  margin: 5px;
  color:white;
  border: none;
  border-radius: 0;
  width: auto;
  padding: 10px;
  float: left;
  font-size: 16px;
}
select input{
  margin: 5px;
  color:black;
  border: none; 
  border-radius: 0;
  width: 12%;
  padding: 1px;
  float: left;
  font-size: 16px;

}
.m-write{
   margin: 5px;
  color:black;
  border: none; 
  border-radius: 0;
  width: 12%;
  padding: 1px;
  float: left;
  font-size: 16px;

}
</style>
</head>
<script>
$(document).ready(function(){
$(".add_exp1").click(function(){
 
  var html =     '<div class="Input_Skill">'+
                 '<input type="text" id="skill_name" placeholder="Enter skill" name="skill_name[]" required>'+
                 '<select input type="text" id="status" class ="js-example-basic-multiple"  multiple="multiple" placeholder="Enter status" name="status[]" required><option value="Beginner">Beginner</option><option value="Intermediate">Intermediate</option><option value="Expert">Expert</option></select>'+
                 '<select input type="text" name="experience[]" class ="js-example-basic-multiple"  multiple="multiple" placeholder="experience" required>'+
                 '@foreach($exper as $experience)'+
                 '<option value="{{$experience->experience}}">{{$experience->experience}}</option>'+
                 '@endforeach <select>'+
                 '<i class="fa fa-close shosha btn_remove" name="remove" style="font-size:36px"></i>'+
                 '</div>';


  $("#admi").append(html);
}); 
$(document).on('click', '.btn_remove', function(){  
           var button_id = $(this).attr("id");   
           $('#row'+button_id+'').remove();  
      }); 
}); 

</script>
<body>

<div id="myDIV" class="header">
<a href="{{ route('employee.index')}}" class="backBtn">back</a>
<span onclick="newElement()" class="add_exp1 btn addBtn">Add Skills</span>
  <h2 style="margin:5px">Update Employee</h2>
  <form class="form-horizontal" action="{{route('employee.update',[$user->id])}}" method="post">
  @method('PATCH')
  @csrf
  <input type="text" placeholder="Employee Name" name="name" value="{{ $user->name}}">
  @error('name')
  <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <input type="text" placeholder="Mobile No." name="mobile" value="{{ $user->mobile}}">
  @error('mobile')
  <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <input type="text" placeholder="designation" name="designation" value="{{ $user->designation}}">
  @error('designation')
  <div class="alert alert-danger">{{ $message }}</div>
  @enderror
 
  @foreach($skill as $key =>  $user2)   
  <div class="Input_Skill">
                 <input type="text" id="skill_name" placeholder="Enter skill" name="skill_name[]" value="{{ $user2->skill_name}}" required>
                 <select input type="text" id="status" class ="m-write js-example-basic-multiple "  multiple="multiple" placeholder="Enter status" name="status[]" value="" required >
                 <option value="">Select status </option>
                 <option value="Beginner" {{ ($user2->status) == 'Beginner' ? 'selected' : ''}}>Beginner</option>
                 <option value="Intermediate" {{ ($user2->status) == 'Intermediate' ? 'selected' : ''}}>Intermediate</option>
                 <option value="Expert" {{ ($user2->status) == 'Expert' ? 'selected' : ''}}>Expert</option></select>
                 <select input type="text" name="experience[]"  multiple="multiple" class ="m-write js-example-basic-multiple" placeholder="experience" value="{{ $user2->experience}}" required>
                   @foreach($exper as $experience)
                 <option value="{{$experience->experience}}" @if($experience->experience == $user2->experience) selected @endif>{{$experience->experience}}</option>
                 @endforeach
                </select>
                 <!-- <i class="fa fa-close shosha btn_remove" name="remove" style="font-size:36px"></i> -->
                 </div> @endforeach
  

<script>$(document).ready(function() {
    $('.js-example-basic-multiple').select2();
});</script>
            
<div id="admi">   

 <br>
</div>
<br><br>
<button type="submit" class="btn addBtn">Submit</button>

</form>
</div>



</body>
</html>
