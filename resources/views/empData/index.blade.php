<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href='https://fonts.googleapis.com/css?family=Sofia' rel='stylesheet'>
<style>
body {
  margin: 0;
  min-width: 250px;
  font-family: 'Sofia';font-size: 22px;
}

table, td, th {  
  border: 1px solid #ddd;
  text-align: left;
}

table {
  border-collapse: collapse;
  width: 100%;
}

th, td {
  padding: 15px;
}

/* Include the padding and border in an element's total width and height */
* {
  box-sizing: border-box;
}
  



/* Style the close button */
.close {
  position: absolute;
  right: 0;
  top: 0;
  padding: 12px 16px 12px 16px;
}

.close:hover {
  background-color: #f44336;
  color: white;
}

/* Style the header */
.header {
  background-color: #f44336;
  padding: 30px 40px;
  color: white;
  text-align: center;
}
a{
  text-decoration: none;
  color:white;
  font-family: 'Sofia';font-size: 22px;
}

button {
  background-color: #04AA6D;
  color: white;
  padding: 10px 18px;
  margin: 8px 0;
  border: none;
  cursor: pointer;
  width: auto;
  font-family: 'Sofia';font-size: 22px;
}

a:hover {
  opacity: 0.8;
}

.cancelbtn {
  width: auto;
  padding: 10px 18px;
  background-color: #f44336;
}
.viewbtn {

  width: auto;
  padding: 10px 18px;
  background-color: #e7e7e7;
   color: black;
}
.addbtn {

width: auto;
padding: 10px 10px;
background-color: #e7e7e7;

 color: black;
}


.addBtn {
  padding: 10px;
  width: auto;
  background: #d9d9d9;
  color: #555;
  float: left;
  text-align: center;
  font-size: 16px;
  cursor: pointer;
  transition: 0.3s;
  border-radius: 0;
}

.addBtn:hover {
  background-color: #bbb;
}
.alert-success{
  padding: 10px;
  width: auto;
  background: #d9d9d9;
  color: green;
  text-align: center;
  font-size: 16px;
  cursor: pointer;
  transition: 0.3s;
  border-radius: 0;
}

.alert-error{
  padding: 10px;
  width: auto;
  background: #d9d9d9;
  color: pink;
  text-align: center;
  font-size: 16px;
  cursor: pointer;
  transition: 0.3s;
  border-radius: 0;
}
</style>

</head>
<body>

<div id="myDIV" class="header">
<a href="{{ route('employee.create')}}" class="addBtn">Add</a>
  <h2 style="margin:5px">Employee List</h2>
</div>
@if (Session::has('success'))
            <div class="alert alert-success text-center">
                <p1>{{ Session::get('success') }}</p1>
            </div>
            @endif 
            @if (Session::has('error'))
            <div class="alert alert-error">
              <p>{{session::get('error')}}</p>
            </div>
            @endif
<table>
    <th>Employee name </th>
    <th>Designation</th>
    <th>Mobile No.</th>
    <th></th>
    <th></th>
  </tr>

  @foreach($data as $user)
  <tr>
    <td>{{ $user->name}}</td>
    <td>{{ $user->designation}}</td>
    <td>{{ $user->mobile}}</td>
    <td><a href="{{route('employee.show',[$user->id])}}" class="viewbtn">view</td>
    <td>
      <form action="{{ route('employee.destroy',$user->id) }}" method="Post">
      <button class="btn btn-primary"><a href="{{ route('employee.edit',$user->id) }}">Edit</a></button>
@csrf
@method('DELETE')
<button type="submit" class=" cancelbtn">Delete</button>
</form>


    </td>
  </tr>
  @endforeach
</table>


</script>

</body>
</html>
