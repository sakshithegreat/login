<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href='https://fonts.googleapis.com/css?family=Sofia' rel='stylesheet'>
<style>
body {
  margin: 0;
  min-width: 250px;
  font-family: 'Sofia';font-size: 22px;
}

table, td, th {  
  border: 1px solid #ddd;
  text-align: left;
}

table {
  border-collapse: collapse;
  width: 100%;
}

th, td {
  padding: 15px;
}

/* Include the padding and border in an element's total width and height */
* {
  box-sizing: border-box;
}
  



/* Style the close button */
.close {
  position: absolute;
  right: 0;
  top: 0;
  padding: 12px 16px 12px 16px;
}

.close:hover {
  background-color: #f44336;
  color: white;
}

/* Style the header */
.header {
  background-color: #f44336;
  padding: 30px 40px;
  color: white;
  text-align: center;
}

a {
  background-color: #04AA6D;
  color: white;
  padding: 10px 18px;
  margin: 8px 0;
  border: none;
  cursor: pointer;
  width: auto;
}

a:hover {
  opacity: 0.8;
}

.showdata {

width: auto;
padding: 10px 10px;
float: right;
 color: black;
}


.addBtn {
  padding: 10px;
  width: auto;
  background: #d9d9d9;
  color: #555;
  float: left;
  text-align: center;
  font-size: 16px;
  cursor: pointer;
  transition: 0.3s;
  border-radius: 0;
}

.addBtn:hover {
  background-color: #bbb;
}
</style>

</head>
<body>

<div id="myDIV" class="header">
<a href="{{ route('employee.index')}}" class="addBtn">back</a>
  <h2 style="margin:5px">{{ $employee->name}} Details</h2>
  <h3 class="showdata">Contact NO. {{ $employee->mobile}}</h3>
</div>

<table>
  <tr>
    <th>Skill Name </th>
    <th>Status</th>
    <th>experience</th>
    <th></th>
  </tr>
  @foreach($skill as $user)
  <tr>
    <td>{{ $user->skill_name}}</td>
    <td>{{ $user->status}}</td>
    <td>{{ $user->experience}}</td>
  </tr>
  @endforeach

</table>

</body>
</html>
