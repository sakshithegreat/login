<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<link href='https://fonts.googleapis.com/css?family=Sofia' rel='stylesheet'>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha256-aAr2Zpq8MZ+YA/D6JtRD3xtrwpEz2IqOS+pWD/7XKIw=" crossorigin="anonymous" />

    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha256-OFRAJNoaD8L3Br5lglV7VyLRf0itmoBzWUoM+Sji4/8=" crossorigin="anonymous"></script>

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>

    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />

    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>




<style>
body {
  margin: 0;
  min-width: 250px;
  font-family: 'Sofia';font-size: 22px;
}

/* Include the padding and border in an element's total width and height */
* {
  box-sizing: border-box;
}

/* Style the close button */
.close {
  position: absolute;
  right: 0;
  top: 0;
  padding: 12px 16px 12px 16px;
}

.close:hover {
  background-color: #f44336;
  color: white;
}

/* Style the header */
.header {
  background-color: #f44336;
  padding: 30px 40px;
  color: white;
  text-align: center;
}

/* Clear floats after the header */
.header:after {
  content: "";
  display: table;
  clear: both;
}

/* Style the input */
input ,select {
  margin: 5px;
  color:black;
  border: none;
  border-radius: 0;
  width: 30%;
  padding: 10px;
  float: left;
  font-family: 'Sofia';font-size: 22px;
  font-size: 18px;
}

/* Style the "Add" button */
.addBtn {
  padding: 10px;
  width: auto%;
  background: #d9d9d9;
  color: #555;
  float: right;
  text-align: center;
  font-size: 16px;
  cursor: pointer;
  transition: 0.3s;
  border-radius: 0;
}

.addBtn:hover {
  background-color: #bbb;
}
.addSkills input{
  margin: 5px;
  color:black;
  border: none;
  border-radius: 0;
  width: auto;
  padding: 10px;
  float: left;
  font-family: 'Sofia';font-size: 22px;
  font-size: 16px;
}
.shosha{
  margin: 5px;
  color:white;
  border: none;
  border-radius: 0;
  width: auto;
  padding: 10px;
  float: left;
  font-size: 16px;
}
.backBtn{
  padding: 10px;
  width: auto%;
  background: #d9d9d9;
  color: #555;
  float: left;
  text-align: center;
  font-size: 16px;
  cursor: pointer;
  transition: 0.3s;
  border-radius: 0;
}
/* 
.custom-select {
  position: relative;
  font-family: Arial;
}

.custom-select select {
  display: none;
}

.select-selected {
  background-color: DodgerBlue;
}


.select-selected:after {
  position: absolute;
  content: "";
  top: 14px;
  right: 10px;
  width: 0;
  height: 0;
  border: 6px solid transparent;
  border-color: #fff transparent transparent transparent;
}


.select-selected.select-arrow-active:after {
  border-color: transparent transparent #fff transparent;
  top: 7px;
}


.select-items div,.select-selected {
  color: #ffffff;
  padding: 8px 16px;
  border: 1px solid transparent;
  border-color: transparent transparent rgba(0, 0, 0, 0.1) transparent;
  cursor: pointer;
  user-select: none;
}


.select-items {
  position: absolute;
  background-color: DodgerBlue;
  top: 100%;
  left: 0;
  right: 0;
  z-index: 99;
}


.select-hide {
  display: none;
}

.select-items div:hover, .same-as-selected {
  background-color: rgba(0, 0, 0, 0.1);
} */
</style>
</head>
<script>

// // Get the modal
// var modal = document.getElementById('id02');

// // When the user clicks anywhere outside of the modal, close it
// window.onclick = function(event) {
//     if (event.target == modal) {
//         modal.style.display = "none";
//     }
//   }

$(document).ready(function(){
$(".add_exp1").click(function(){
 
  var html =     '<div onclick="deleterow(this)" class="Input_Skill">'+
                 '<input type="text" id="skill_name" placeholder="Enter skill" name="skill_name[]" required>'+
                 '<select input type="text" id="status" placeholder="Enter status" name="status[]" required><option value="Beginner">Beginner</option><option value="Intermediate">Intermediate</option><option value="Expert">Expert</option></select>'+
                 '<select input type="text" name="experience[]" class ="m-write js-example-basic-multiple" placeholder="experience" multiple required>'+
                 '@foreach($exper as $experience)'+
                 '<option value="{{$experience->experience}}">{{$experience->experience}}</option>'+
                 '@endforeach <select>'+
                 '<i class="fa fa-close shosha btn_remove" name="remove" style="font-size:36px"></i>'+
                 '</div>';


  $("#admi").append(html);
}); 
$(document).on('click', '.btn_remove', function(){  
         //  var button_id = $(this).attr("id");   
           //$('#row'+button_id+'').remove();  
           function deleterow(e){
        $(e).parent().parent().parent().remove();
    }

      }); 
}); 



</script>
<body>

<div id="myDIV" class="header">
<span onclick="newElement()" class="add_exp1 btn addBtn">Add Skills</span>
<a href="{{ route('employee.index')}}" class="backBtn">back</a>
  <h2 style="margin:5px">Add Employee</h2>
  <form class="form-horizontal"  action="{{route('employee.store')}}" method="post">
  @csrf
  <input type="text" placeholder="Employee Name" name="name">
  @error('name')
  <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <input type="text" placeholder="Mobile No." name="mobile">
  @error('mobile')
  <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <input type="text" placeholder="designation" name="designation">
  @error('designation')
  <div class="alert alert-danger">{{ $message }}</div>
  @enderror

  
<div id="admi">    
 <br>
</div>
<br><br>
<button type="submit" class="btn addBtn">Submit</button>

</form>
</div>

<script type="text/javascript">

$(document).ready(function() {

    $('.category').select2();

});

</script>


</body>
</html>
