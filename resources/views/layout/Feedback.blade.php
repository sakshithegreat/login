@include('layout.head')
@include('layout.navbar')
<br>
<br>
<br><br>
<button class="w3-button w3-black w3-section" type="button"><a href="{{ url('layout/accountpage/')}}">Add Feedback</a>
      </button>
<h1>I am a Feedback page</h1>


<div class="w3-row-padding w3-grayscale">
@foreach($test as $feedback)
    <div class="w3-col l3 m6 w3-margin-bottom">
      <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAdVBMVEX///8AAAC+vr7Z2dny8vK2trZ9fX1PT0/8/PygoKCRkZGurq5ubm67u7vPz8+cnJzg4ODm5uaQkJAODg7u7u6oqKhYWFiIiIidnZ3JyckeHh51dXUoKCiEhIQ1NTVjY2NpaWk7OztFRUVJSUkhISEWFhYwMDCGg9+6AAAFRklEQVR4nO2ci1biMBRFjbRYBHk/BlAUdfz/TxyUQamW9py2uYlrnf0BWTltcl+5ydWVEEIIIYQQQgghhBBCCCGEEEIIIYQQQgghhIiOLBmm/UVvtOpOR71F/66TZaGn1B7z9ah7437w+vjQ6yShJ9eYZL0qEPfFZtnr/OafmZaqOzF7WoeeaC2y4RaR95/VOPR8WZLRPaHvnZf0N63WZDoj9b3z1g89b5T5Qw15H8x6v+E/Zrd19X1ovA49/0rWuyYCDyzjtjnJU0N979yGVlHCXQv6Dmw6oYVcIOu2I/DAIrSWQib71gQ6N4jQqLa0Qk/cR2dwRu0KPBBZsLpqXaBzd6FFncME2Tjx2Jts6UWgc9PQyk74EujcKLS0IwNvAp2LIt3wYWS+iCASb99N5BmGFph6FujcJKzAiXeBbh82gGuaDSJ0QwpsL5soIw0n0P8mPBJsK86NBLrnUAofrRSGilDXZgLdbB5CYPZmp9BtQyj0HczkCVCcMvD159zbK/QbcP/EPARPjAXae4yptULrylRmLtDd2CqsaUhvuqP+db+wcaEa00yxli9cpl+dF0nKF3dMfeKQ1zf4Hj5P6PqOZaJI10d3RUtsSGaXhlkUbWe2xZ8/477Ui51CNi+8nBosqHHs8kTSSpStLupj/TFTyAksr+r2iZHMXCKXGFYdyxO9G3+t0kSqn2RZORyx5q2c/p4QOKvur0zwBiqjwygqrUB8GG5tqhdEKzDbEMt5nuHxbDpue4RCrPjQaXm8phCBCLqqYGNjE7gRtg/NWuGFb3IonOCZ0w4eFI3BBx6FfUIU2fAo6w84oknJDTcLRKQMfzaPwj65hgVuiFE34JgWWTDun5lGUTQStEig8JSOKeKiK8PCIeJlNiYAQUNBi9gbrgUz2xDeiBZ1YfiuARcmg3GERcMi3J3A9VCAw1qEbfA/5JI5cPFb/EP4WI2LIUEDZnHIBlsarr0AdEIWlgb2FlznJFhzs/AWcPnPyz+0iGngUxnudg8YtnkSlWOMKlxRw4IGzJOoHHCi80ANizmhV0+iciTo/dcnaljs1hs3Zl3QOo2PuNSmeR+utTHZKngkaVNrgxNExneBFtrmvhfsLnrEoFiZGTgFaYMMPUnZE4PuoREfvYnKAx8z4F8cTPGtLkLB54d4qgPez7Q6P4Q3In4qjTVJvZo99oIqhCtjYJXZaht6KGREVMI4gpe9sdYC9FqD3YtEGVqDB8+KwA43u0VKVKOgnYie9Vg2QuPnT8hBPupfvcuqMykkD0aLd7YvghDtaFVuH36Mwfh+F/FMUnk+ABdFTA64z2BaTso+Pn5mbn1rhmqivTw53GRZuoojVO/rpWCE2M72b51w7fqFbdBME7T9L2RboYssKvOkTZALiNS1kKJ4BA9vAz2QQd3Pa6bwLdCdfOaCXjOFwd4bIm6ENFIY7s0BogGsiUKTw4oL4JFNE4VBnzeBe00beAumsNw+2Sswxd0gLY5Ixum2eitbR9w/Jlk1wf20PGTu3Jbnmrvgr++Vb6btGJhgpyR6M7smU0JJ58ItWhxLLpbRo3hc8FL7yQPVm1hc3Ar+StSRwh6iR/brjwvi3EgEFh7V1DHxP9rZI3oc8rvnr/k25zhfaY7mD76TNzf7uuX3+Zl73ERhZL44P3F7qe/Csv1pkF0EbiLP5LPNppGPPnXrxPjM7qkLZdascjv/qMTG83RpjrQVA3iIx99ife76avLSRlFs2Y1xhZ5YhCmKWRLz5xdCCCGEEEIIIYQQQgghhBBCCCGEEEIIIYQQQgghhBBCiEj4BzybQnvIQiyyAAAAAElFTkSuQmCC" alt="John" style="width:100%">
      <h3>{{$feedback->name}}</h3>
      <p class="w3-opacity">{{$feedback->subject}}</p>
      <p>{{$feedback->comment}}.</p>
      <p><button class="w3-button w3-light-grey w3-block">Contact</button></p>
    </div>
    @endforeach
  </div>

  <br>
  <Br>
  @include('layout.footer')
