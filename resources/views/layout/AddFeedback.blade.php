@include('layout.head')
@include('layout.navbar')
<br>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
  <style>
<style>
table, td, th {  
  border: 1px solid #ddd;
  text-align: left;
}

table {
  border-collapse: collapse;
  width: 100%;
}

th, td {
  padding: 15px;
}
</style>
</head>
<body>

<!-- Contact Section -->
 <div class="w3-container w3-padding-32" id="contact">

  <div class="w3-bar w3-white w3-wide w3-padding w3-card">

  

    <a href="#" class="w3-bar-item">Hey {{$user->name}}  </a>
     
    <!-- Float links to the right. Hide them on small screens -->
      <div class="w3-right w3-hide-small">
         <a href="{{url('layout/Logout/')}}" class="w3-bar-item w3-button">Logout</a>
      </div>
  </div>
   
    <h3 class="w3-border-bottom w3-border-light-grey w3-padding-16">Add Feedback</h3>
    <p>Lets get in touch by giving Feedback, that help in our next project.</p>


    @if (session()->has('success'))
            <div class="alert alert-success">{{session('success')}}</div>
            @endif
            @if (session()->has('error'))
            <div class="alert alert-error">{{session('error')}}</div>
            @endif
    <form action="{{url('layout/store/')}}" method="post" target="_blank">
    @csrf
      <input class="w3-input w3-border" type="text" placeholder="Name" required name="name">
                    @error('name')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
      <input class="w3-input w3-section w3-border" type="text" placeholder="Subject" required name="subject" >
      <input class="w3-input w3-section w3-border" type="text" placeholder="Comment" required name="comment" >
                    @error('comment')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
      <button class="w3-button w3-black w3-section" type="submit">
        <i class="fa fa-paper-plane"></i>submit
      </button>
    </form>

<table>
  <tr>
    <th>name</th>
    <th>Subject</th>
    <th>Comment</th>
  </tr>
  @foreach($test as $feedback)
  <tr>
    <td>{{$feedback->name}}</td>
    <td>{{$feedback->subject}}</td>
    <td>{{$feedback->comment}}</td>
  </tr>
  @endforeach
</table>
  <br>
  <Br>
  @include('layout.footer')

</body>
</html>