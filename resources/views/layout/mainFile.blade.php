<!doctype html>

<html>

<head>

   @include('layout.head')


</head>

<body>

<div class="container">

   <header class="row">
       @include('layout.sidebar')
       @include('layout.navbar')

   </header>

   <div id="main" class="row">

           @yield('content')
           <!-- Page content -->
<div class="w3-content w3-padding" style="max-width:1564px">
</div>

   </div>

   <footer class="row">

       @include('layout.footer')

   </footer>

</div>

</body>

</html>