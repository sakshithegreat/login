<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>Edit List</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <style>

    </style>
</head>
<body>
<form method="post" action="{{ route('list.update',[$tedit->id])}}">
@method('PATCH')
@csrf

<div class="container m-5 p-2 rounded mx-auto bg-light shadow">
    <!-- App title section -->
    <div class="row m-1 p-4">
        <div class="col">
            <div class="p-1 h1 text-primary text-center mx-auto display-inline-block">
                <i class="fa fa-check bg-primary text-white rounded p-2"></i>
                <u>My Todo-s</u>
            </div>
        </div>
    </div>
    <!-- Create todo section -->

 <div class="mb-3">
  <label for="Title" class="form-label">Title</label>
  <input type="text" class="form-control" placeholder="add title" name="name" value = "{{$tedit->name}}">
  @error('name')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
</div>
 <div class="mb-3">
  <label for="Discription" class="form-label">Discription</label>
  <textarea type="text"  class="form-control" rows="3" name="content" value ="">{{$tedit->content}}</textarea>
  @error('content')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
</div>
 <br>
    <button type="submit" class="btn btn-primary">Save Change</button>
</div>
</form>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

</body>
</html>