<!DOCTYPE html>
<html lang="en">
<head>
  <title>List Data</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <h2>List Data</h2>
             
  <table class="table">
    <thead>
      <tr>
        <th>S No.</th>
        <th>Title</th>
        <th colspan="2">content</th>
      </tr>
    </thead>
    <tbody>
    @foreach($data as $listdata)
      <tr>
      <th scope="row">{{ $loop->iteration}}</th>
        <td>{{$listdata->name}}</td>
        <td colspan="2">{{$listdata->content}}</td>
        <td>
        <a href="{{ route('list.edit',[$listdata->id])}}" class="btn btn-primary btn-sm">edit</a>
        <a href="{{ url('deleteuser/'.$listdata->id)}}" calss="btn btn-danger btn-sm"> Delete</a>
     
      </td>
      </tr>
      @endforeach
     
    </tbody>
  </table>
  <div>
     <a href="{{ route('list.create')}}" class="btn btn-primary btn-sm"> Add List </a>
     <a href="{{ url('/logout')}}" class="btn btn-primary btn-sm"> Logout </a>
    </div>


  @foreach($test as $user)
  @foreach($user->getTodoList as $todo)
  {{$todo->name}}
  @endforeach
  @endforeach
</div>

</body>
</html>