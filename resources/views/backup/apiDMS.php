<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Support\Facades\Validator;
use Hash;


class ForgetPasswordController extends Controller
{
    // public function forgetPassword(Request $request){
        
    //     $validator = Validator::make($request->all(), [
    //         'mobile_number' => 'required',
    //     ]);
        
    //     if($validator->fails()){
    //         return response()->json(['success'=>false, 'message' => $validator->errors()->first()]);
    //         }
    //         $user = User::where('mobile_number', $request->mobile_number)->first();

    //         if (!$user){
    //             return response()->json([
    //                 'message' => 'We can\'t find a user with that mobile number.'
    //             ]);
    //         }
    //         $user->email;
    //         return response();

        
    // }

    public function forgetPassword (Request $request){
        $validator = Validator::make($request->all(), [
            'mobile_number' => 'required',
            'password' => 'required|min:6|confirmed',
            'password_confirmation' => 'required',
        ]);
        
        if($validator->fails()){
            return response()->json(['success'=>false, 'message' => $validator->errors()->first()]);
            }
        
        $user = User::where('mobile_number', $request->mobile_number)->first();
        

        if (!$user)
        try{
            return response()->json([
                'message' => 'We can\'t find a user with that mobile number.'
            ]);
        }
        catch(Exception $e){
            $user->password = Hash::make($request->password_confirmation);
            $user->save();
        }

        $response['status'] = true;
        $response['message'] = "Password updated successfully.";
 
         return response()->json($response);

    }

    public function passwordReset(Request $request){

          $mobile_no = $request->input("mobile_number");

        $validator = Validator::make($request->all(), [
            'mobile_number' => 'required',
            'old_password' => 'required',
            'password' => 'required|confirmed',
            'password_confirmation' => 'required',
        ]);
        
        if($validator->fails()){
            return response()->json(['success'=>false, 'message' => $validator->errors()->first()]);
            }

      
      $user = User::where('mobile_number', $request->mobile_number)->first();
     // $user = User::find($mobile_no);
      dd($user);
        if (!$user)
        try{
            return response()->json([
                'message' => 'We can\'t find a user with that mobile number.'
            ]);
        }
        catch(Exception $e){


      $old_password = $request->input('old_password');
      $password = $request->input('password');
     // $user = User::find($mobile_no);

       
        if(Hash::check($old_password, $user->password)){
            $user->password =Hash::make($password);
            $user->save;
        }
        
           // $user->password = Hash::make($request->password_confirmation);
           // $user->save();
        }

        $response['status'] = true;
        $response['message'] = "Password updated successfully.";
 
         return response()->json($response); 
    }



//     public function newForgetPassword(){

//     }

//     public function newPassword(Request $request){

// $rules = [
//     "password_token" => "required|string|max:8",
//     "password" => "required|confirmed|string|max:45",
// ];

// $validator = Validator::make($request->all(),$rules);
// if($validator->fails())
// return $this->errorMessage(true, $validator->error()->all());

// $data = $validator->validated();

// $VerifToken = APIPasswordResetTokenModel::where([
//     ["token_signature", hash('md5',$data['password_reset_code'])],
//     ["token_type", APIPasswordResetTokenMOdel::PASSWORD_VERIF_TOKEN]
// ])->get();

// if($VerifToken == null || $VerifToken->count() <= 0){
//   return $this->error_message = "Invaalid  token for resetting password";
    
// }
// $User = $VerifToken[0]->UserInfo;
// if($User == null || $User->count() <= 0){
//     return $this->errorMessage(true, "Token does not correspond to any existing to any existing user");
// } else if (Carban::now()->greaterThan($VerifToken->expires_at)) 
//    return $this->errorMessage(true,"The reset password token has expired");
    

// $new_password = Hash::make($data["password"]);
// $User->password = $new_password;
// $User->save();
//     $VerifToken[0]->update([
//         "expires_at" =>Carbon::now(),
//     ]);
//     return $this->errorMessage(false, "success", ["user"=>$User]);     
//     }


//     public function resetPasswordToken(){

//         $rules = [
//             "email" => "require|email|exits:User,email",
//         ];

//         $validator = Validator::make($request->all(), $rules);
      
//         if($validator->fails()){
//             return $this->errorMessage(false, $validator->errors()->first());
//             }

//             $data = $validator->validated();

//             $user = User::where("email",$data["email"])->first();

//             $reset_link_sent = $user->sendPasswordResetLink();
//             if($reset_link_sent){
//                 return $this->errorMessage(false,"a password reset token has been sentto your email, please enter the password resent page to reset your password");
//             }
//             return $this->errorMessage(true,$user->getErrorMessage());
//         }



//         do {
//             $token = $this->getResetCode();
//             $signature = hash('md5',$token);
//             $exists = $this->where([
//                 ["user_id" , $user->id],
//                 ["token_signature", $signature]
//             ])->exists();
//         }while (exists);

//         try{
//             $user->notify(new APIPasswordResetNotification($token));
//             return $this->create([
//                 "user_id" => $user->id,
//                 "token_signature" => $signature,
//                 "expires_at" => Carbon::now()->addMinutes(30),
//             ]);
//         } catch (|Throwable $th)  {
//             $this->error_message = $th->getMessage();
//             return false;
//         }

            


//             public function validatePasswordResetToken($data)
//             {
//                 $resetToken = APIPasswordResetTokenMOdel::where([
//                     ["token_signature", hash('md5',$data['password_reset_code'])],
//                     ["token_type", APIPasswordResetTokenMOdel::PASSWORD_RESSET_TOKEN]
                
//                 ])->first();
                
//                 if($resetToken == null || $resetToken->count() <= 0){
//                     $this->error_message = "Invaalid password reset code";
//                     return false;

//                 }
//                 if (Carban::now()->greaterThan($resetToken->expires_at)) {
//                     $this->error_message = "Invalid password reset code";
//                     return false;
//                 }
//                 $reset_token = $resetToken->getResetIdentifierCode();
//                 if($reset_token) {
//                     $resetToken->update([
//                         "expires_at" =>Carbon::now(),
//                     ]);
//                     return [
//                         "token" =>$reset_token,
//                     ];
//                 }
//                 else {
//                     $this->error_message = $resetToken->getErrorMessage();
//                     return false;
//                 }
                
//             }


//             public function getResetIdentifierCode(){
//                 $token = $this->genResetCode();
//                 try {
//                     $this->create([
//                  "user_id" => $this->user_id,
//                  "token_signature" => hash('md5', $token),
//                  "used_token" => $this->id, 
//                  "token_type" => APIPasswordResetTokenMOdel::PASSWORD_VERIF_TOKEN,
//                  "expires_at" => Carbon::now()->addMinutes(30),
//                     ]);
//                     return $token;
//                 } catch (|Throwable $th) {
//                     $this->error_message = $th->getMessage();
//                     return false;
//                 }

//             }



        
    }

   //if((Hash::check(request('old_password'), Auth::user()->password)) == false)
//}