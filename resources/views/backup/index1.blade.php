@extends('website.master')
@section('content')


    <body id="page-top">
        <!-- Navigation-->
     
       
        <!-- Portfolio Section-->
        @include('website.portfoliyo1')
        <!-- About Section-->


       @include('website.about')
        <!-- Contact Section-->
        @include('website.contact')

        <!-- Portfolio Modals-->
        
        @include('website.port')
        <!-- Bootstrap core JS-->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
        <!-- Core theme JS-->
        <script src="{{asset('js/scripts.js')}}"></script>
        <!-- * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *-->
        <!-- * *                               SB Forms JS                               * *-->
        <!-- * * Activate your form at https://startbootstrap.com/solution/contact-forms * *-->
        <!-- * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *-->
        <script src="https://cdn.startbootstrap.com/sb-forms-latest.js"></script>
    </body>
</html>

        @endsection