<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\UserLayout;
use App\Models\TodoList;
use App\Models\Layout;
use vallidator;
use Hash;



class layoutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       // $test = UserLayout::get();
        //return view('UserLayout.add',compact('test'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
   
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

// layout controllers

public function layout(Request $request)
{
    //$user = UserLayout::where('user_id',$request->id)->get();
    // dd($user);
   return view('layout/home');
 
}

public function layoutFeedback(Request $request)
{

    $test = UserLayout::get();
    //dd($test);
   return view('layout/Feedback',compact('test'));
}


public function layoutAbout(Request $request)
{
   return view('layout/about');
}

public function layoutProject(Request $request)
{

   
   return view('layout/Project');
}
public function layoutContact(Request $request)
{
   return view('layout/Contact');
}

public function layoutAddFeedback(Request $request)
{
    //dd('You are active');
    $user = User::with('getUserlayout')->where('id',Auth::user()->id)->first();
    $test = UserLayout::where('user_id',$user->id)->get();

   // $data = TodoList::with('getUserlayout')->where('user_id','getUserlayout.user_id')->get();

  // $testdata = UserLayout::where('user_id',$user->id)->where('name',Auth::user()->name)->get();
 //dd($testdata);
 //$collection = UserLayout::with('getTodoList')->where('user_id','getTodoList.user_id')->get();


//  $collection = User::whereHas('getTodoList', function ($query) {
//     $query->where('user_id',Auth::user()->id);
//    })->get();
// dd($collection);

    return view('layout/AddFeedback',compact('user','test'));
   
  

}

public function layoutAccountpage(Request $request)
{
    //return "sakshi";
 
   return view('layout/login');

}

public function storedata(Request $request)
    {
        //dd($request->all());
        $validated = $request->validate([
            'name' => 'required',
            'subject'=>'required',
            'comment' => 'required',
        ]);

        UserLayout::create([
           'name'=> $request->name,
           'subject'=>$request->subject,
           'comment'=> $request->comment,

        ]);
     
        return redirect('layout/AddFeedback');
       
    }

    public function layoutSignup(Request $request)
    {
        //dd($request->all());
        $credentials = $request->validate([
            'name' => 'required',
            'email'=>'required',
            'password' => 'required',
        ]);

        User::create([
           'name'=> $request->name,
           'email'=>$request->email,
           'password' => Hash::make($request->password),
        ]);
        
        return redirect('layout/AddFeedback');
       
    }

    public function layoutLogin(Request $request)
    {
        //return (sakshi);
        $request->validate([
            'email' => ['required'],
           'password' => ['required'],
       ]);

       $credentials = $request->only('email', 'password');

       if (Auth::attempt($credentials)) {
          //if(Auth::user()->roll == 1){
           return redirect('layout/AddFeedback');
       }
    //}
      return redirect()->back()->with('error','Login failed');
    }

    public function layoutLogout(Request $request){
        Auth::logout();
        return redirect('layout');
    }

}


