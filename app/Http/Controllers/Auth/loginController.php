<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\User;
use vallidator;
use Hash;


class loginController extends Controller
{
       public function login(Request $request){
        return view('Auth.auth');
    }

    public function authUser(Request $request)
{

        $request->validate([
             'email' => ['required'],
            'password' => ['required'],
        ]);

        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            // Authentication passed...
            return redirect('/list');
        }
       return redirect()->back()->with('error','Login failed');

}

        public function userLogout(Request $request){
            Auth::logout();
            return redirect('/');

        }

    public function userSignup(Request $request)
    {
        $credentials = $request->validate([
           'email' => 'required|unique:users',
           'password' => 'required',
       ]);
       User::create([
              'email' => $request->email,
              'password' => Hash::make($request->password),
              'roll'=> true,
            ]);
      return redirect()->back()->with('success','Register Successfully!');
    }

    public function signup(){
        return view('Auth.signup');
    }


        //Test Users

            public function testusers (Request $request){
            return view('admin.activeUser');
        }


        public function testauthUser(Request $request)
        {
            dd($request->all());
        
                $request->validate([
                     'email' => ['required'],
                     'roll'=>['required'],
                    'password' => ['required'],
                ]);
        
                $credentials = $request->only('email','roll', 'password');
        
                if (Auth::attempt($credentials)) {
                    // Authentication passed...
                    return view('admin.testUser');
                }
               return redirect()->back()->with('error','Test Login failed');
        
        }
                public function testuserLogout(Request $request){ 
                    Auth::logout();
                    return redirect('/');
        
                }

                public function home(Request $request){ 
                    return view('admin.testUser');
        
                }

                // layout controllers

                public function layout(Request $request)
                {
                   return view('layout/home');
                }

                public function website(Request $request)
                {
                   return view('website/index');
                }

                public function websiteContect(Request $request)
                {
                   return view('website/contact');
                }


                public function websiteAbout(Request $request)
                {
                   return view('website/about');
                }

                public function websitePortfolio(Request $request)
                {
                   return view('website/portfoliyo1');
                }


}
