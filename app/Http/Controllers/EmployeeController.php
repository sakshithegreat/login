<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Employee;
use App\Models\Skill;
use App\Models\Experience;
use vallidator;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data=employee::all();
        return view('empData.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $exper= Experience::all();
        return view('empData.employee',compact('exper'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validated = $request->validate([
            'status' => 'required',
            'skill_name' => 'required',
            'experience' => 'required',
            'name' => 'required',
            'mobile' => 'required',
            'designation' => 'required'
        ]);
        
        $employee = Employee::create([
            'name' => $request->name,
            'mobile'=> $request->mobile,
            'designation' =>$request->designation,
        ]);
        $skill_name = $request->skill_name;
     $status = $request->status;
     $experience = $request->experience;

    if($employee->id){
    foreach($skill_name as $key => $skill_name) {
        $data =[
            'employee_id' => $employee->id,
            'status'=> $request->status[$key],
            'skill_name'=>$skill_name,
            'experience'=> $request->experience[$key],
        ];
        Skill::create($data);
    }
}
     
     //  return redirect()->route('employee.index')->with('message', 'The success message!');
       return redirect('employee')->with('success', 'New Data has been added successfully.');

    return redirect()
    ->back()
    ->withInput()
    ->with('error', 'There was a failure while sending the message!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $employee = employee::find($id);
        $skill = Skill::where('employee_id',$employee->id)->get();
        return view('empData.viewdata',compact('employee','skill'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = Employee::with('getskill')->find($id);
        $skill = Skill::where('employee_id',$user->id)->get();
        $exper= Experience::all();
        //dd($exper);
        return view('empData.update',compact('user','skill','exper'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $user = Employee::find($id);
       $user->name = $request->name;
       $user->mobile = $request->mobile;
       $user->designation = $request->designation;
       $user->update();
    if($user->id){
        $skillDelete = Skill::where('employee_id',$user->id)->delete();
        if($skillDelete){
            foreach($request->skill_name as $key=>$skill){
                $data2 = new Skill();
                $data2->employee_id = $user->id;
                $data2->skill_name = $skill;
                $data2->status = $request->status[$key];
                $data2->experience =  $request->experience[$key];
                $data2->save();


                // $data2->status = implode(',',$request->status);
                // $data2->experience =  implode(',',$request->experience);
                // $data2->save();
            }
        }
    }

    //return redirect()->route('employee.index')->with('message', 'The success message!');
    return redirect('employee')->with('success', 'Data has been Updated successfully.');

    return redirect()
    ->back()
    ->withInput()
    ->with('error', 'There was a failure while sending the message!');


 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $employee =Employee::where('id',$id)->first();
        $employeeDelete  = $employee->delete();
        if($employeeDelete){
            Skill::where('employee_id',$employee->id)->delete();
        }
        return redirect()->route('employee.index')
        ->with('success', 'employee data deleted successfully');
    }


    // public function deleteEmployee($id)
    // {
    //     dd('hello');
    //     $employee = employee::find($id);
    //     $skill = Skill::where('employee_id',$employee->id)->get();
    //     $skill->delete();
    //     $employee->delete();
    //     return view('empData.viewdata');
    // }
}