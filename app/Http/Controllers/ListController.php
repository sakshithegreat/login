<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TodoList;
use validator;
use App\Models\User;


class ListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
     
        $test = User::get();
        $data=TodoList::orderBy('id', 'desc')->get();
        return view('TodoList.index',compact('data','test'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $test = User::get();
        return view('TodoList.add',compact('test'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'name' => 'required|unique:todo_lists|max:15',
            'content' => 'required',
        ]);

        TodoList::create([
           'name'=> $request->name,
           'content'=> $request->content,
        ]);
        //return view('TodoList.add');
        return redirect('list');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tedit=TodoList::find($id);
        return view('TodoList.edit',compact('tedit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validated = $request->validate([
            'name' => 'required|max:3',
            'content' => 'required',
        ]);

        $uedit = TodoList::find($id);
        $uedit->name = $request->name;
        $uedit->content = $request->content;
        $uedit->save();
        return redirect('list');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function destroy($id)
    // {
    //     $duser = TodoList::find($id);
    //     $duser->destroy();
        
    //     return redirect('list');
    // }

    public function deleteUser(Request $request){

        $user = TodoList::find($request->id);
        $user->delete();

        return redirect()->to('list');

    }

}
