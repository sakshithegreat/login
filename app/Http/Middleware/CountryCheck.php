<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CountryCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {

        if (auth()->user()->roll==1) {

            return $next($request);
        }
        else {
            return response()->json('User Roll no. is not equal to 1.');
        }

        
    }
}
