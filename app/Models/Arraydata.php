<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Arraydata extends Model
{
    use HasFactory;
    protected $fillable = [
      'id',
      'admission_id',
      'name'
    ];
}
