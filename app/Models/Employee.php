<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    use HasFactory;
    protected $fillable=[
        'name',
        'mobile',
        'designation'
    ];
    public function getskill()
    {
    	return $this->hasMany(Skill::class,'employee_id','id');
    }
}
